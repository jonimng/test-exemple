<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Activity;

/* @var $this yii\web\View */
/* @var $model app\models\Activity */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activity-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            [ // the status name 
                'label' => $model->attributeLabels()['statusid'],
                'value' => $model->statusItem->name,    
            ],
            [ // the owner name of the lead
                'label' => $model->attributeLabels()['Categoryid'],
                'value' => $model->categoryItem->name, 
                 
            ],
        ],
    ]) ?>

</div>
