<?php
namespace app\rbac;
use app\models\User;
use yii\rbac\Rule;
use Yii; 
use yii\db\ActiveRecord;

class OwnCategoryRule extends Rule
{
	public $name = 'ownCategoryRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			if(isset($_GET['id'])){
			    $checkUser = User::findOne($user);
			        
			    if($checkUser->CategoryId == $_GET['id'])
			        return true;
			}
		}
		return false;
	}
}